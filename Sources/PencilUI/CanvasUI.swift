import SwiftUI
import PencilKit

@available(iOS 13.0, *)
@available(macOS 10.15, *)
public struct CanvasView {
  @Binding public var canvasView: PKCanvasView
    public init(canvasView: Binding<PKCanvasView>) {
        self._canvasView = canvasView
    }
}

@available(iOS 13.0, *)
extension CanvasView: UIViewRepresentable {
    public func makeUIView(context: Context) -> PKCanvasView {
      canvasView.tool = PKInkingTool(.marker, color: .black, width: 10)
        if #available(iOS 14.0, *) {
#if targetEnvironment(simulator)
            canvasView.drawingPolicy = .anyInput
#endif
        } else {
            // Fallback on earlier versions
        }
      canvasView.backgroundColor = .white
    return canvasView
  }

    public func updateUIView(_ uiView: PKCanvasView, context: Context) {}
}
